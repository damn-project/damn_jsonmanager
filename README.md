Damn JSON manager
=================

**Divide and map. Now.** -- the damn project -- helps mappers by dividing some
big area into smaller squares that a human can map.

`damn-manager.js` lets users create and update areas.

License
-------

The project is published under [MIT License][].

[MIT License]: ./LICENSE

Discussion
----------

Use the [mailing list][] for the discussion.

[mailing list]: https://lists.sr.ht/~qeef/damn-project
