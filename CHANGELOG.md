Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

Unreleased
==========

Added
-----

- Area form (used when creating/updating area.)


0.2.1 - 2020-12-09
==================

Fixed
-----

- Missing documentation on archiving areas.
- Latitude/longitude input text title.


0.2.0 - 2020-12-08
==================

Added
-----

- Custom square size.

Changed
-------

- Readme and changelog format.


0.1.5 - 2020-10-17
==================

Added
-----

- Error output when creating/posting new area.


0.1.4 - 2020-10-01
==================

Fixed
-----

- Update to new server (v0.6.0) API.


0.1.3 - 2020-01-31
==================

Fixed
-----

- Avoid html in area JSON with `xmp` html tag.


0.1.2 - 2020-01-30
==================

Added
-----

- Create area procedure.


0.1.1 - 2020-01-25
==================

Fixed
-----

- Scroll to top when restored.


0.1.0 - 2020-01-24
==================

Added
-----

- Changelog, license, readme.
- Basic functionality.
