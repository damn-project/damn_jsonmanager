// -- global library variables
DAMN_SERVER = "https://server.damn-project.org"

function html_area(area)
{
    var nof_squares = area["squares_to_map"];
    nof_squares += area["squares_to_review"];
    nof_squares += area["squares_done"];
    var to_map = 100 * area["squares_to_map"] / nof_squares;
    var to_rev = 100 * area["squares_to_review"] / nof_squares;
    var done = 100 * area["squares_done"] / nof_squares;
    var row = "<div";
            row += " class='row text'";
            row += " onclick='goto_area(" + area["aid"] + ")'";
            row += " title='AID (Priority) (To map/To review/Done):";
                row += " Changeset comment'";
        row += ">";
        row += area["aid"];
        row += " (" + area["priority"] + ")";
        row += " (" + Math.floor(to_map) + "%";
        row += "/" + ((to_rev > 0) ? (Math.floor(to_rev) + 1) : 0) + "%";
        row += "/" + Math.floor(done) + "%";
        row += ")";
        row += ": " + area["tags"];
    row += "</div>";
    return row;
}
function load_areas()
{
    var ih = "";
    var u = load_user();
    if (u) {
        ih += u["display_name"] + " is managing.";
        ih += " <a href='javascript:authenticate()'>";
                ih += "Authenticate again";
        ih += "</a>.";
    } else {
        ih += "<a href='javascript:authenticate()'>";
                ih += "Authenticate to OpenStreetMap";
        ih += "</a>.";
    }
    up("user", ih);

    get_areas(function(r) {
        var ih = "";
        ih += "<h2>Create new</h2>";
        ih += "<div class='button' onclick='create_area()'>";
        ih += "Create new area";
        ih += "</div>";
        ih += "<h2>Change active</h2>";
        var divided = false;
        for (var a in r) {
            if (!divided && r[a]["priority"] < 0) {
                ih += "<h2>Change archived</h2>";
                divided = true;
            }
            ih += html_area(r[a]);
        }
        up("main", ih);
    });
}

function html_commit(commit, version)
{
    var cm = JSON.parse(commit["message"]);
    cm["aid"] = commit["aid"];
    var d = new  Date(commit["date"]);
    var ih = "";
    ih += "<div";
        ih += " class='commit'";
        ih += " onclick='restore_area(" + commit["cid"] + ")'";
        ih += " title='Version " + version + ".'";
    ih += ">";
        ih += "<pre id='" + commit["cid"] + "' style='display: none;'>";
        ih += JSON.stringify(cm, null, 4);
        ih += "</pre>";

        ih += "Backup version " + version;
        ih += " of area " + commit["aid"];
        ih += " created by ";
            ih += "<a";
                ih += " onclick='event.stopPropagation()'";
                ih += " href='https://www.openstreetmap.org/message/new/";
                    ih += commit["author"];
                    ih += "'";
                ih += " target='_blank'";
            ih += ">";
                ih += commit["author"];
            ih += "</a>";
        ih += " on " + d.toLocaleString();
        ih += ":<br />";

        ih += "<ul>";
        ih += "<li>Changeset comment:&nbsp;" + cm["tags"] + "</li>";
        ih += "<li>Priority:&nbsp;" + cm["priority"] + "</li>";

        ih += "<li>Description:<ul>";
        for (i in cm["description"]) {
            ih += "<li>";
            ih += "<em>" + i + "</em>:&nbsp;";
            ih += cm["description"][i];
            ih += "</li>";
        }
        ih += "</ul></li>";

        ih += "<li>Instructions:<ul>";
        for (i in cm["instructions"]) {
            ih += "<li>";
            ih += "<em>" + i + "</em>:&nbsp;";
            ih += cm["instructions"][i];
            ih += "</li>";
        }
        ih += "</ul></li>";

        ih += "</ul>";
    ih += "</div>";
    return ih;
}
function html_area_form(a={})
{
    var ih = "";
    if (a["aid"])
        ih += "<input id='aid' type='hidden' value='" + a["aid"] + "' />";
    ih += "<h2>Changeset comment</h2>";
    ih += "<textarea";
    ih += " id='tags'";
    ih += " placeholder='Changeset comment #with #tags'";
    ih += " class='tags'";
    ih += ">";
    if (a["tags"])
        ih += a["tags"];
    ih += "</textarea>";
    ih += "<h2>Priority:&nbsp;";
    ih += "<input";
    ih += " id='priority'";
    ih += " type='text'";
    if (a["priority"])
        ih += " value='" + a["priority"] + "'";
    ih += " title='Priority'";
    ih += " placeholder='Priority'";
    ih += " />";
    ih += "</h2>";
    ih += "<p>";
    ih += "Set the priority to <em>less than zero</em> to archive the area:";
    ih += "<ul>";
    ih += "<li>-1: The area is archived but kept.</li>";
    ih += "<li>-10: The area is permanently deleted within 10 minutes.</li>";
    ih += "</ul>";
    ih += "</p>";
    ih += "<h2>Description</h2>";
    ih += "<p>";
    ih += "Specify language and write the description of what to be done.";
    ih += " Any mapper can add translation of the description in web client.";
    ih += " To delete the description, delete both fields.";
    ih += "</p>";
    ih += "<div id='desc_list'>";
    if (a["description"]) {
        for (var i in a["description"]) {
            ih += "<p>";
            ih += "<input";
                ih += " id='lang " + i + "'";
                ih += " type='text'";
                ih += " value='" + i + "'";
                ih += " title='Language'";
                ih += " placeholder='Language'";
            ih += " />";
            ih += "<textarea";
            ih += " id='desc " + i + "'";
            ih += " placeholder='Description'";
            ih += ">";
            ih += a["description"][i];
            ih += "</textarea>";
            ih += "</p>";
        }
    }
    ih += "</div>";
    ih += "<a href='javascript:add_desc()'>+ add description</a>";
    ih += "<h2>Instructions</h2>";
    ih += "<p>";
    ih += "Put one world describing what to map.";
    ih += " Put some (wiki) link to more information.";
    ih += " To delete the instruction, delete the first field.";
    ih += "</p>";
    ih += "<ul id='inst_list'>";
    if (a["instructions"]) {
        for (var i in a["instructions"]) {
            ih += "<li>";
            ih += "<input";
                ih += " id='inst " + i + "'";
                ih += " type='text'";
                ih += " value='" + i + "'";
                ih += " title='What to map?'";
                ih += " placeholder='What to map?'";
            ih += " />";
            ih += "<input";
                ih += " id='inst_d " + i + "'";
                ih += " type='text'";
                ih += " value='" + a["instructions"][i] + "'";
                ih += " title='Link to wiki'";
                ih += " placeholder='Link to wiki'";
                ih += " size='40'";
            ih += " />";
            ih += "</li>";
        }
    }
    ih += "</ul>";
    ih += "<a href='javascript:add_inst()'>+ add instruction</a>";
    return ih;
}
function add_inst()
{
    var ul = document.getElementById("inst_list");
    var li = document.createElement("li");
    var t1 = document.createElement("input");
    t1.id = "inst " + ul.childElementCount;
    t1.type = "text";
    t1.title = "What to map?";
    t1.placeholder = "What to map?";
    li.appendChild(t1);
    var t2 = document.createElement("input");
    t2.id = "inst_d " + ul.childElementCount;
    t2.type = "text";
    t2.title = "Link to wiki";
    t2.placeholder = "Link to wiki";
    t2.size = 40;
    li.appendChild(t2);
    ul.appendChild(li);
}
function add_desc()
{
    var div = document.getElementById("desc_list");
    var p = document.createElement("p");
    var t1 = document.createElement("input");
    t1.id = "lang " + div.childElementCount;
    t1.type = "text";
    t1.title = "Language";
    t1.placeholder = "Language";
    p.appendChild(t1);
    var t2 = document.createElement("textarea");
    t2.id = "desc " + div.childElementCount;
    t2.placeholder = "Description";
    p.appendChild(t2);
    div.appendChild(p);
}
function area_from_html()
{
    var a = {};
    try {
        a["aid"] = Math.floor(document.getElementById("aid").value);
    } catch (e) {
        // No aid means creating new area.
    }
    a["tags"] = document.getElementById("tags").value;
    a["priority"] = Math.floor(document.getElementById("priority").value);
    a["description"] = {}
    var inputs = document.getElementsByTagName("input");
    for (var i in inputs) {
        if (inputs[i].id && inputs[i].id.substring(0, 5) == "lang ") {
            var lang = inputs[i].id.slice(5);
            var lang_div = document.getElementById("lang " + lang);
            var desc = document.getElementById("desc " + lang);
            if (lang_div.value != "" && desc.value != "")
                a["description"][lang_div.value] = desc.value;
        }
    }
    a["instructions"] = {}
    for (var i in inputs) {
        if (inputs[i].id && inputs[i].id.substring(0, 5) == "inst ") {
            var id2 = inputs[i].id.slice(5);
            var what = document.getElementById("inst " + id2);
            var link = document.getElementById("inst_d " + id2);
            if (what.value != "")
                a["instructions"][what.value] = link.value;
        }
    }
    return a;
}
function goto_area(aid)
{
    window.scrollTo(0, 0);
    var ih = "";
    ih += "<div id='info'></div>";
    ih += "<div class='button' onclick='update_area(" + aid + ")'>";
        ih += "Update area " + aid;
    ih += "</div>";
    ih += "<div class='button' onclick='load_areas()'>";
        ih += "Go back to list of areas";
    ih += "</div>";
    ih += "<div id='commits'></div>";
    up("main", ih);
    get_area(function(r) {
        up("info", html_area_form(r));
    }, aid);
    get_commits(function(r) {
        var ih = "";
        var commits = [];
        for (var c in r) {
            if (r[c]["sid"] == null)
                commits.push(r[c]);
        }
        ih += "<h2>Click to restore old version</h2>";
        ih += "<p>";
        ih += "You must click <em>Update area "+aid+"</em> to save changes!";
        ih += "</p>";
        ih += "<div";
        ih += " class='commit'";
        ih += " onclick='goto_area(" + aid + ")'";
        ih += " title='Restore current version'";
        ih += ">";
            ih += "Restore current version (download again from the server.)";
        ih += "</div>";
        for (var c in commits) {
            ih += html_commit(commits[c], (commits.length - c));
        }
        up("commits", ih);
    }, aid, "2020-01-01T00:00:00");
}
function update_area(aid)
{
    var area = area_from_html();
    if (area["aid"] != aid) {
        up("info", "Area ID does not match!");
        return false;
    }
    put_area(
        function(r) {
            goto_area(r["aid"]);
        },
        area,
        function (r) {
            up("info", "FAILED, server reply: " + JSON.stringify(r));
        },
    );
}
function restore_area(cid)
{
    window.scrollTo(0, 0);
    var ra = JSON.parse(document.getElementById(cid).innerHTML);
    up("info", html_area_form(ra));
}

function post_area()
{
    up("info", "Sending data...");
    var area = area_from_html();
    var fc_source = document.getElementById("featurecollection").files[0];
    var fr = new FileReader();
    fr.onload = function(e) {
        area["featurecollection"] = JSON.parse(e.target.result);
        // TODO needs improvement in `lib.js` of the damn client
        // see https://client.damn-project.org/lib.js
        var http_request = new XMLHttpRequest();
        if (document.getElementById("is_ss").checked) {
            var ssh = document.getElementById("ssh").value;
            var ssw = document.getElementById("ssw").value;
            var q = "?ssh=" + (1.0*ssh) +"&ssw=" + (1.0*ssw);
            http_request.open("POST", ep("/areas" + q), true);
        } else {
            http_request.open("POST", ep("/areas"), true);
        }
        http_request.setRequestHeader("Content-Type", "application/json");
        http_request.setRequestHeader("Authorization", "Bearer "+load_token());
        http_request.responseType = "json";
        http_request.onreadystatechange = function() {
            var done = 4, ok = 201;
            if (
                http_request.readyState === done
                && http_request.status === ok
            ) {
                goto_area(http_request.response["aid"]);
            } else if (http_request.readyState === done) {
                up("info", "FAILED: " + JSON.stringify(http_request.response));
            }
        };
        http_request.send(JSON.stringify(area));
    };
    try {
        fr.readAsText(fc_source);
    } catch(e) { up("info", "Problem loading GeoJSON file."); }
}
function create_area()
{
    window.scrollTo(0, 0);
    var ih = "";
    ih += "<div id='info'></div>";
    ih += html_area_form();
    ih += "<h2>GeoJSON boundary file</h2>";
    ih += "<p>";
    ih += "This has to be GeoJSON FeatureCollection.";
    ih += " If the FeatureCollection has member 'name'";
    ih += " or the first Feature has member 'properties' with member 'name',";
    ih += " then <em>divide to squares</em> function is NOT used.";
    ih += " In such a case it's expected you already divided the area.";
    ih += " You may create GeoJSON file at";
    ih += " <a href='http://geojson.io/' target='_blank'>";
        ih += "http://geojson.io/";
    ih += "</a>.";
    ih += "</p>";
    ih += "<input id='featurecollection' type='file' />";
    ih += "<p>";
    ih += "Advanced:<ul>";
    ih += "<li>";
        ih += "<input type='checkbox' id='is_ss' onclick='enable_ss()' />";
        ih += "Use custom square size (lat x lon): ";
        ih += "<input";
        ih += " type='text'";
        ih += " id='ssh'";
        ih += " title='Latitude in degrees, float.'";
        ih += " value='0.004'";
        ih += " size='4'";
        ih += " onchange='compute_ss()'";
        ih += "/>";
        ih += "x";
        ih += "<input";
        ih += " type='text'";
        ih += " id='ssw'";
        ih += " title='Longitude in degrees, float.'";
        ih += " value='0.004'";
        ih += " size='4'";
        ih += " onchange='compute_ss()'";
        ih += "/>";
        ih += " ~ (width x height) ";
        ih += "<span id='computed_ss'></span>";
    ih += "</li>";
    ih += "</ul>";
    ih += "</p>";
    ih += "<div class='button' onclick='post_area()'>";
        ih += "Post new area to server";
    ih += "</div>";
    ih += "<div class='button' onclick='load_areas()'>";
        ih += "Go back to list of areas";
    ih += "</div>";
    up("main", ih);
    enable_ss();
    compute_ss();
}
function compute_ss()
{
    var ssh = document.getElementById("ssh").value;
    var ssw = document.getElementById("ssw").value;
    var computed_ss = document.getElementById("computed_ss");
    // see https://stackoverflow.com/questions/1253499/simple-calculations-for-working-with-lat-lon-and-km-distance
    var km_h = 110.574 * ssh;
    var km_w = 111.320 * ssw * Math.cos(ssh * Math.PI / 180);
    var sq = km_h * km_w;
    var r = "";
    r += km_w.toFixed(2) + " km x " + km_h.toFixed(2) + " km";
    r += " = " + sq.toFixed(2) + " km^2";
    computed_ss.innerHTML = r;
}
function enable_ss()
{
    var enable = document.getElementById("is_ss").checked;
    var ssh = document.getElementById("ssh").disabled = !enable;
    var ssw = document.getElementById("ssw").disabled = !enable;
}
